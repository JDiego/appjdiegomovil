﻿using AppJDiegoMovil.Interfaces.SQLite;
using AppJDiegoMovil.UWP.SQLite;
using System.IO;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(FicConfigSQLiteUWP))]
namespace AppJDiegoMovil.UWP.SQLite
{
    public class FicConfigSQLiteUWP : IFicConfigSQLiteNETStd
    {
        public string FicGetDatabasePath()
        {
            return Path.Combine(ApplicationData.Current.LocalFolder.Path, AppSettings.FicDatabaseName);
        }//TRAER LA RUTA FISICA DONDE SE GUARDA LA BASE DE DATOS EN UWP
    }//CLASS
}//NAMESPACE
