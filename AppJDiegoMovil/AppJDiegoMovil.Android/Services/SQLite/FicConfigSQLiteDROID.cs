﻿using AppJDiegoMovil.Interfaces.SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppJDiegoMovil.Droid.Services.SQLite;

using Xamarin.Forms;

[assembly: Dependency(typeof(FicConfigSQLiteDROID))]
namespace AppJDiegoMovil.Droid.Services.SQLite
{
    public class FicConfigSQLiteDROID : IFicConfigSQLiteNETStd
    {
        public string FicGetDatabasePath()
        {
            var FicPathFile = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
            var FicDirectorioDB = FicPathFile.Path;
            FicDirectorioDB = FicDirectorioDB;// + "/CocacolaNay/";
            string FicPathDB = Path.Combine(FicDirectorioDB, AppSettings.FicDatabaseName);
            return FicPathDB;
        }//TRAER LA RUTA FISICA DONDE ESTARA LA BASE DE DATOS SQLITE
    }//CLASS
}//NAMESPACE