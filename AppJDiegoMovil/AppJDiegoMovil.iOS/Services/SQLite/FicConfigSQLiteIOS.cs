﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AppJDiegoMovil.Interfaces.SQLite;
using AppJDiegoMovil.iOS.Services.SQLite;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(FicConfigSQLiteIOS))]
namespace AppJDiegoMovil.iOS.Services.SQLite
{
    class FicConfigSQLiteIOS : IFicConfigSQLiteNETStd
    {
        public string FicGetDatabasePath()
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, AppSettings.FicDatabaseName);
        }//TRAER LA RUTA FISICA DONDE SE GUARDA LA BASE DE DATOS EN UWP
    }//CLASS
}//NAMESPACE
