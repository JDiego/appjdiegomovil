﻿using System;
using Microsoft.EntityFrameworkCore;
using AppJDiegoMovil.Models.Asistencia;
using Xamarin.Forms;

namespace AppJDiegoMovil.Data
{
    public class FicDBContext : DbContext
    {
        private readonly string FicDataBasePath;

        public FicDBContext(string FicPaDataBasePath)
        {
            FicDataBasePath = FicPaDataBasePath;
            FicMetCrearDB();
        }

        public FicDBContext(DbContextOptions<FicDBContext> ficOptions) : base(ficOptions)
        {

        }

        public async void FicMetCrearDB()
        {
            try
            {
                //FIC: SE crea la base de datos en base al esquema
                await Database.EnsureCreatedAsync();
            }
            catch (Exception err)
            {
                await new Page().DisplayAlert("ALERTA", err.Message.ToString(), "OK");
            }
        }//ESTE METODO CREA LA BASE DE DATOS

        protected async override void OnConfiguring(DbContextOptionsBuilder FicPaOptionBuilder)
        {
            try
            {
                FicPaOptionBuilder.UseSqlite($"Filename={FicDataBasePath}");
                FicPaOptionBuilder.EnableSensitiveDataLogging();
            }
            catch (Exception err)
            {
                await new Page().DisplayAlert("ALERTA", err.Message.ToString(), "OK");
            }
        }//CONFIGURACION DE LA CONECXION

        //GESTION DE INVENTARIOS
        public DbSet<eva_cat_edificios> eva_cat_edificios { get; set; }
        public DbSet<eva_cat_espacios> eva_cat_espacios { get;  set; }
        public DbSet<cat_estatus> cat_estatus { get; set; }
        public DbSet<cat_tipo_estatus> cat_tipo_estatus { get; set; }

        protected async override void OnModelCreating(ModelBuilder modelBuilder)
        {
            try
            {
                //EVA_CAT_EDIFICIOS
                modelBuilder.Entity<eva_cat_edificios>().HasKey(pk => new { pk.IdEdificio });

                //EVA_CAT_ESPACIOS
                modelBuilder.Entity<eva_cat_espacios>().HasKey(pk => new { pk.IdEspacio });
                modelBuilder.Entity<eva_cat_espacios>().HasOne(fk => fk.eva_cat_edificios).WithMany().HasForeignKey(fk => new { fk.IdEspacio});
                modelBuilder.Entity<eva_cat_espacios>().HasOne(fk => fk.cat_estatus).WithMany().HasForeignKey(fk => new { fk.IdTipoEstatus, fk.IdEstatus});

                //CAT_ESTATUS
                modelBuilder.Entity<cat_estatus>().HasKey(pk => new { pk.IdEstatus, pk.IdTipoEstatus });
                modelBuilder.Entity<cat_estatus>().HasOne(fk => fk.cat_tipo_estatus).WithMany().HasForeignKey(fk => new { fk.IdTipoEstatus });


                //CAT_TIPO_ESTATUS
                modelBuilder.Entity<cat_tipo_estatus>().HasKey(pk => new { pk.IdTipoEstatus});
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
            }
        }//AL CREAR EL MODELO

    }
}
