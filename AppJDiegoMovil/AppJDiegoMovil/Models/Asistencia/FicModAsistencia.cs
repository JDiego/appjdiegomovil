﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppJDiegoMovil.Models.Asistencia
{
    public class cat_estatus
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 IdEstatus { get; set; }

        public cat_tipo_estatus cat_tipo_estatus { get; set; }
        public Int16 IdTipoEstatus { get; set; }

        [StringLength(50, ErrorMessage = "Clave no puede ser más de 50 caracter")]
        public String Clave { get; set; }
        [StringLength(30, ErrorMessage = "Clave no puede ser más de 30 caracter")]
        public String DesEstatus { get; set; }
        public bool Activo { get; set; } = false;
        public DateTime FechaReg { get; set; } = DateTime.Now;
        public DateTime FechaUltMod { get; set; } = DateTime.Now;
        [StringLength(20, ErrorMessage = "UsuarioReg no puede ser más de 20 caracter")]
        public String UsuarioReg { get; set; }
        [StringLength(20, ErrorMessage = "UsuarioMod no puede ser más de 20 caracter")]
        public String UsuarioMod { get; set; }
        public bool Borrado { get; set; } = false;
    }

    public class cat_tipo_estatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 IdTipoEstatus { get; set; }
        [StringLength(30, ErrorMessage = "DesTipoEstatus no puede ser más de 1 caracter")]
        public String DesTipoEstatus { get; set; }
        public bool Activo { get; set; } = false;
        public DateTime FechaReg { get; set; } = DateTime.Now;
        [StringLength(20, ErrorMessage = "UsuarioReg no puede ser más de 20 caracter")]
        public String UsuarioReg { get; set; }
        public DateTime FechaUltMod { get; set; } = DateTime.Now;
        [StringLength(20, ErrorMessage = "UsuarioMod no puede ser más de 20 caracter")]
        public String UsuarioMod { get; set; }
        public bool Borrado { get; set; } = false;
    }

    public class eva_cat_edificios
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 IdEdificio { get; set; }
        [StringLength(10, ErrorMessage = "Alias no puede ser más de 10 caracteres")]
        public String Alias { get; set; }
        [StringLength(50, ErrorMessage = "DesEdificio no puede ser más de 50 caracteres")]
        public String DesEdificio { get; set; }
        public Int16? Prioridad { get; set; }
        [StringLength(20, ErrorMessage = "Clave no puede ser más de 20 caracteres")]
        public String Clave { get; set; }
        public DateTime? FechaReg { get; set; } = DateTime.Now;
        public DateTime? FechaUltMod { get; set; } = DateTime.Now;
        [StringLength(20, ErrorMessage = "UsuarioReg no puede ser más de 20 caracteres")]
        public String UsuarioReg { get; set; }
        [StringLength(20, ErrorMessage = "UsuarioMod no puede ser más de 20 caracteres")]
        public String UsuarioMod { get; set; }
        public bool Activo { get; set; } = false;
        public bool Borrado { get; set; } = false;

        //public List<eva_cat_espacios> IdEspacio { get; set; }
    }

    public class eva_cat_espacios
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 IdEspacio { get; set; }

        public Int16 IdEdificio { get; set; }
        public eva_cat_edificios eva_cat_edificios { get; set;}
        
        [StringLength(20, ErrorMessage = "Clave no puede ser más de 20 caracteres")]
        public String Clave { get; set; }
        [StringLength(50, ErrorMessage = "DesEspacio no puede ser más de 50 caracteres")]
        public String DesEspacio { get; set; }
        public Int16? Prioridad { get; set; }
        [StringLength(10, ErrorMessage = "Alias no puede ser más de 10 caracteres")]
        public String Alias { get; set; }
        public Int16? RangoTiempoReserva { get; set; }
        public Int16? Capacidad { get; set; }

        public cat_estatus cat_estatus { get; set; }
        public Int16? IdTipoEstatus { get; set; }
        public Int16? IdEstatus { get; set; }
        //public cat_tipo_estatus cat_tipo_estatus { get; set; }

        [StringLength(255, ErrorMessage = "RefeUbicacion no puede ser más de 255 caracteres")]
        public String RefeUbicacion { get; set; }
        [StringLength(1, ErrorMessage = "PermiteCruce no puede ser más de 1 caracter")]
        public String PermiteCruce { get; set; }
        [StringLength(20, ErrorMessage = "Observacion no puede ser más de 20 caracter")]
        public String Observacion { get; set; }
        public DateTime? FechaReg { get; set; } = DateTime.Now;
        public DateTime? FechaUltMod { get; set; } = DateTime.Now;
        [StringLength(20, ErrorMessage = "UsuarioReg no puede ser más de 20 caracter")]
        public String UsuarioReg { get; set; }
        [StringLength(20, ErrorMessage = "UsuarioMod no puede ser más de 20 caracter")]
        public String UsuarioMod { get; set; }
        public bool Activo { get; set; } = false;
        public bool Borrado { get; set; } = false;
    }
}
