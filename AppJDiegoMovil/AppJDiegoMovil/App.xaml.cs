﻿using AppJDiegoMovil.ViewModels.Base;
using Xamarin.Forms;



namespace AppJDiegoMovil
{
	public partial class App : Application
	{
        private static FicViewModelLocator ficVmLocator;
        // FIC: Metodo
        public static FicViewModelLocator FicMetLocator
        {
            get { return ficVmLocator = ficVmLocator ?? new FicViewModelLocator(); }
        }

		public App ()
		{
			InitializeComponent();

            //MANDAMOS NUESTRO MAESTRO DETALLE COMO MAINPAGE
            //MainPage = new NavigationPage(new Views.CatGenerales.FicViCatEdificiosList(null));
            MainPage = new Views.Navegacion.FicMasterPage();

            //MainPage = new Views.MainPage();
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
