﻿using System.Threading.Tasks;

namespace AppJDiegoMovil.Interfaces.WebApi
{
    public interface IFicSrvExportarWebApi
    {
        Task<string> FicPostExportEdificios();
    }//INTERFACE
}//NAMESPACE
