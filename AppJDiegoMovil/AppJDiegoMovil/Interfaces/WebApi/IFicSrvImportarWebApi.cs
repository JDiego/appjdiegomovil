﻿using System.Threading.Tasks;

namespace AppJDiegoMovil.Interfaces.WebApi
{
    public interface IFicSrvImportarWebApi
    {
        Task<string> FicGetImportEdificios(int id = 0);

        //Task<string> FicGetImportInventarios(int id = 0);
        //Task<string> FicGetImportCatalogos();
    }//INTERFACE
}//NAMESPACE
