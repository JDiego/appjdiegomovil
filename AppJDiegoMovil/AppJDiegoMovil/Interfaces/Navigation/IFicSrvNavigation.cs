﻿using System;

namespace AppJDiegoMovil.Interfaces.Navigation
{
    public interface IFicSrvNavigation
    {
        /*METODOS PARA LA NAVEGACION ENTRE VIEWS DE LA APP*/
        void FicMetNavigateTo<TDestinationViewModel>(object navigationContext = null, bool show = true);
        void FicMetNavigateTo<TDestinationViewModel>(object navigationContext = null);
        void FicMetNavigateTo(Type destinationType, object navigationContext = null);
        void FicMetNavigateBack();
    }//INTERFACE
}//NAMESPACE