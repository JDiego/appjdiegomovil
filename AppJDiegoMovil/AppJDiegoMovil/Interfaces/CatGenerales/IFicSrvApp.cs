﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppJDiegoMovil.Models.Asistencia;
using System;
namespace AppJDiegoMovil.Interfaces.CatGenerales
{
    public interface IFicSrvApp
    {
        //METODOS DE LA TABLA EVA_CAT_EDIFICIOS
        Task FicMetInsertEdificio(eva_cat_edificios ficPa_eva_Cat_Edificios);    // CREATE ITEM
        Task<IList<eva_cat_edificios>> FicMetGetListEdificios();                 // READ LIST
        Task<eva_cat_edificios> FicMetGetItemEdificioById(Int16 id);             //  "   ITEM
        Task FicMetUpdateEdificio(eva_cat_edificios ficPa_eva_Cat_Edificios);    // UPDATE ITEM
        Task FicMetRemoveEdificio(eva_cat_edificios ficPa_eva_Cat_Edificios);    // DELETE ITEM
    }
}
