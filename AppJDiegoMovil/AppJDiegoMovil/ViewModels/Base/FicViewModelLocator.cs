﻿using Autofac;
using AppJDiegoMovil.ViewModels.CatGenerales;
using AppJDiegoMovil.Services.Navigation;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.Services.CatGenerales;
using AppJDiegoMovil.Interfaces.CatGenerales;
using AppJDiegoMovil.ViewModels.WebApi;
using AppJDiegoMovil.Services.WebApi;
using AppJDiegoMovil.Interfaces.WebApi;

namespace AppJDiegoMovil.ViewModels.Base
{
    public class FicViewModelLocator
    {
        private static IContainer FicContainer;

        public FicViewModelLocator()
        {
            var builder = new ContainerBuilder();

            // ViewModels
            // Registrar cada uno de los ViewModels creados en el siguiente formato
            // builder.RegisterType<ViewModels>();
            builder.RegisterType<FicVmCatEdificiosList>();
            builder.RegisterType<FicVmCatEdificiosItem>();
            builder.RegisterType<FicVmCatEdificiosUpdate>();
            builder.RegisterType<FicVmCatEdificiosInsert>();
            builder.RegisterType<FicVmImportarWebApi>();
            builder.RegisterType<FicVmExportarWebApi>();

            // Servicios
            builder.RegisterType<FicSrvNavigation>().As<IFicSrvNavigation>().SingleInstance();
            builder.RegisterType<FicSrvApp>().As<IFicSrvApp>();
            builder.RegisterType<FicSrvImportarWebApi>().As<IFicSrvImportarWebApi>();
            builder.RegisterType<FicSrvExportarWebApi>().As<IFicSrvExportarWebApi>();

            if (FicContainer != null)
            {
                FicContainer.Dispose();
            }

            FicContainer = builder.Build();
        }

        // Crear los metodos que se mandan llamar desde el archivo xaml.cs de cada vista para
        // ligar el ViewModel con la vista
        public FicVmCatEdificiosList FicVmCatEdificiosList
        {
            get { return FicContainer.Resolve<FicVmCatEdificiosList>(); }
        }
        public FicVmCatEdificiosInsert FicVmCatEdificiosInsert
        {
            get { return FicContainer.Resolve<FicVmCatEdificiosInsert>(); }
        }
        public FicVmCatEdificiosItem FicVmCatEdificiosItem
        {
            get { return FicContainer.Resolve<FicVmCatEdificiosItem>(); }
        }
        public FicVmCatEdificiosUpdate FicVmCatEdificiosUpdate
        {
            get { return FicContainer.Resolve<FicVmCatEdificiosUpdate>(); }
        }
        public FicVmImportarWebApi FicVmImportarWebApi
        {
            get { return FicContainer.Resolve<FicVmImportarWebApi>(); }
        }
        public FicVmExportarWebApi FicVmExportarWebApi
        {
            get { return FicContainer.Resolve<FicVmExportarWebApi>(); }
        }
    }
}
