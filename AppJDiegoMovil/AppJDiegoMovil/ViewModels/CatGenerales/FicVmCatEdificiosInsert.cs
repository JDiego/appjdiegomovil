﻿using AppJDiegoMovil.ViewModels.Base;
using AppJDiegoMovil.Models.Asistencia;
using System.Windows.Input;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.Interfaces.CatGenerales;

namespace AppJDiegoMovil.ViewModels.CatGenerales
{
    public class FicVmCatEdificiosInsert : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvApp FicLoSrvApp;

        private eva_cat_edificios _Edificios;
        public eva_cat_edificios Edificio
        {
            get { return _Edificios; }
            set
            {
                _Edificios = value;
                RaisePropertyChanged();
            }
        }

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateBack();
        }

        private ICommand AddEdificio;
        public ICommand FicMetAddCommand
        {
            get { return AddEdificio = AddEdificio ?? new FicVmDelegateCommand(AddEdificioExecute); }
        }
        private void AddEdificioExecute()
        {
            Edificio.UsuarioMod = Edificio.UsuarioReg;
            FicLoSrvApp.FicMetInsertEdificio(Edificio);
            FicLoSrvNavigation.FicMetNavigateBack();
        }

        public FicVmCatEdificiosInsert(IFicSrvNavigation FicPaSrvNavigation, IFicSrvApp FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public override void OnAppearing(object navigationContext)
        {
            base.OnAppearing(navigationContext);
            _Edificios = new eva_cat_edificios();
        }
    }
}
