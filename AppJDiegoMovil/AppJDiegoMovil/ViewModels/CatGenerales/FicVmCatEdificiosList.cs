﻿using AppJDiegoMovil.ViewModels.Base;
using AppJDiegoMovil.Models.Asistencia;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.Interfaces.CatGenerales;
using Xamarin.Forms;
using AppJDiegoMovil.Views.Navegacion;
using AppJDiegoMovil.Views.CatGenerales;

namespace AppJDiegoMovil.ViewModels.CatGenerales
{
    public class FicVmCatEdificiosList : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvApp FicLoSrvApp;

        public FicVmCatEdificiosList(IFicSrvNavigation FicPaSrvNavigation, IFicSrvApp FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        private ObservableCollection<eva_cat_edificios> _SfDataGrid_ItemSource_Edificios;
        public ObservableCollection<eva_cat_edificios> SfDataGrid_ItemSource_Edificios
        {
            get { return _SfDataGrid_ItemSource_Edificios; }
            set
            {
                _SfDataGrid_ItemSource_Edificios = value;
                RaisePropertyChanged();
            }
        }

        private eva_cat_edificios _SfDataGrid_SelectItem_Edificio;
        public eva_cat_edificios SfDataGrid_SelectItem_Edificio
        {
            get { return _SfDataGrid_SelectItem_Edificio; }
            set
            {
                _SfDataGrid_SelectItem_Edificio = value;
                RaisePropertyChanged();
            }
        }

        private ICommand AddNewEdificio;
        public ICommand FicMetAddCommand
        {
            get { return AddNewEdificio = AddNewEdificio ?? new FicVmDelegateCommand(AddNewEdificioExecute); }
        }
        private void AddNewEdificioExecute()
        {
            //var FicPageOpen = (Page)System.Activator.CreateInstance(typeof(FicViCatEdificiosInsert));
            //FicPageOpen.Title = "";

            /*var ficMasterPage = Application.Current.MainPage as FicMasterPage;
            ficMasterPage.Detail = new NavigationPage(FicPageOpen);
            ficMasterPage.IsPresented = false;*/

            //ficMastePage.Detail = new NavigationPage(new Views.CatGenerales.FicViCatEdificiosInsert(null));

           FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEdificiosInsert>(null);
        }

        private ICommand EditEdificio;
        public ICommand FicMetEditCommand
        {
            get { return EditEdificio = EditEdificio ?? new FicVmDelegateCommand(EditEdificioExecute); }
        }
        private void EditEdificioExecute()
        {
            if (SfDataGrid_SelectItem_Edificio != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEdificiosUpdate>(SfDataGrid_SelectItem_Edificio);
            }
        }

        private ICommand DetailsEdificio;
        public ICommand FicMetDetailsCommand
        {
            get { return DetailsEdificio = DetailsEdificio ?? new FicVmDelegateCommand(DetailsEdificioExecute); }
        }
        private void DetailsEdificioExecute()
        {
            if (SfDataGrid_SelectItem_Edificio != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEdificiosItem>(SfDataGrid_SelectItem_Edificio);
            }
        }

        private ICommand DeleteEdificio;
        public ICommand FicMetDeleteCommand
        {
            get { return DeleteEdificio = DeleteEdificio ?? new FicVmDelegateCommand(DeleteEdificioExecute); }
        }
        private void DeleteEdificioExecute()
        {

        }

        public async override void OnAppearing(object navigationContext)
        {
            base.OnAppearing(navigationContext);
            var resultadoEdificios = await FicLoSrvApp.FicMetGetListEdificios();

            SfDataGrid_ItemSource_Edificios = new ObservableCollection<eva_cat_edificios>();

            foreach (var edifcio in resultadoEdificios)
            {
                SfDataGrid_ItemSource_Edificios.Add(edifcio);
            }
        }

    }
}
