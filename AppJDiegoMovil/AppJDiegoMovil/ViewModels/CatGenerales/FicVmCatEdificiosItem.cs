﻿using AppJDiegoMovil.ViewModels.Base;
using AppJDiegoMovil.Models.Asistencia;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.Interfaces.CatGenerales;

namespace AppJDiegoMovil.ViewModels.CatGenerales
{
    public class FicVmCatEdificiosItem : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvApp FicLoSrvApp;

        private eva_cat_edificios _Edificio;
        public eva_cat_edificios Edificio
        {
            get { return _Edificio; }
            set
            {
                _Edificio = value;
                RaisePropertyChanged();
            }
        }

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        public void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateBack();
        }

        private ICommand EditEdificio;
        public ICommand FicMetEditCommand
        {
            get { return EditEdificio = EditEdificio ?? new FicVmDelegateCommand(EditEdificioExecute); }
        }
        private void EditEdificioExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEdificiosUpdate>(Edificio);
        }

        public FicVmCatEdificiosItem(IFicSrvNavigation FicPaSrvNavigation, IFicSrvApp FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public override void OnAppearing(object navigationContext)
        {
            base.OnAppearing(navigationContext);
            Edificio = navigationContext as eva_cat_edificios;
        }
    }
}