﻿using AppJDiegoMovil.ViewModels.Base;
using AppJDiegoMovil.Models.Asistencia;
using System.Windows.Input;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.Interfaces.CatGenerales;
using System;

namespace AppJDiegoMovil.ViewModels.CatGenerales
{
    public class FicVmCatEdificiosUpdate : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvApp FicLoSrvApp;

        private eva_cat_edificios _Edificios;
        public eva_cat_edificios Edificio
        {
            get { return _Edificios; }
            set
            {
                _Edificios = value;
                RaisePropertyChanged();
            }
        }

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateBack();
        }

        private ICommand UpdateEdificio;
        public ICommand FicMetUpdateCommand
        {
            get { return UpdateEdificio = UpdateEdificio ?? new FicVmDelegateCommand(UpdateEdificioExecute); }
        }
        private void UpdateEdificioExecute()
        {
            Edificio.FechaUltMod = DateTime.Now;
            FicLoSrvApp.FicMetUpdateEdificio(Edificio);
            FicLoSrvNavigation.FicMetNavigateBack();
        }

        public FicVmCatEdificiosUpdate(IFicSrvNavigation FicPaSrvNavigation, IFicSrvApp FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public override void OnAppearing(object navigationContext)
        {
            base.OnAppearing(navigationContext);
            Edificio = (navigationContext as eva_cat_edificios);
        }
    }
}
