﻿using AppJDiegoMovil.Interfaces.WebApi;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppJDiegoMovil.ViewModels.WebApi
{
    public class FicVmImportarWebApi : INotifyPropertyChanged
    {
        private string _FicTextAreaImpEdf, _FicLabelIdEdf;
        private ICommand _FicMecImportIdEdf, _FicMecImportEdf;

        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvImportarWebApi IFicSrvImportarWebApi;
        public FicVmImportarWebApi(IFicSrvNavigation FicLoSrvNavigation, IFicSrvImportarWebApi IFicSrvImportarWebApi)
        {
            this.FicLoSrvNavigation = FicLoSrvNavigation;
            this.IFicSrvImportarWebApi = IFicSrvImportarWebApi;
        }//CONSTRUCTOR

        public string FicTextAreaImpEdf
        {
            get { return _FicTextAreaImpEdf; }
        }

        public string FicLabelIdEdf
        {
            get { return _FicLabelIdEdf; }
            set
            {
                if (value != null) _FicLabelIdEdf = value;
            }
        }

        public void OnAppearing()
        {
        }//METODO QUE SE MANDA A LLAMAR EN LA VIEW

        public ICommand FicMecImportIdEdf
        {
            get
            {
                return _FicMecImportIdEdf = _FicMecImportIdEdf ?? new FicVmDelegateCommand(FicMecImportEdificioId);
            }
        }//ESTE EVENTO AGREGA EL COMANDO AL BOTON EN LA VIEW

        private async void FicMecImportEdificioId()
        {
            try
            {
                if (_FicLabelIdEdf.Length > 0)
                {
                    _FicTextAreaImpEdf = await IFicSrvImportarWebApi.FicGetImportEdificios(int.Parse(_FicLabelIdEdf));
                    RaisePropertyChanged("FicTextAreaImpEdf");
                    await new Page().DisplayAlert("ALERTA", "Datos Actualizados.", "OK");
                }
                else
                {
                    await new Page().DisplayAlert("ALERTA", "ID NO VALIDO.", "OK");
                }
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
            }
        }

        public ICommand FicMecImportInv
        {
            get
            {
                return _FicMecImportEdf = _FicMecImportEdf ??
                      new FicVmDelegateCommand(FicMecImportEdificio);
            }
        }//ESTE EVENTO AGREGA EL COMANDO AL BOTON EN LA VIEW

        private async void FicMecImportEdificio()
        {
            try
            {
                _FicTextAreaImpEdf = await IFicSrvImportarWebApi.FicGetImportEdificios();
                RaisePropertyChanged("FicTextAreaImpInv");
                await new Page().DisplayAlert("ALERTA", "Datos Actualizados.", "OK");
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
            }
        }       

        #region  INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
