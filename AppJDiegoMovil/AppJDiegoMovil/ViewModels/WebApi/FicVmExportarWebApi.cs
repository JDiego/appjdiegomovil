﻿using AppJDiegoMovil.Interfaces.WebApi;
using AppJDiegoMovil.Interfaces.Navigation;
using AppJDiegoMovil.ViewModels.Base;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppJDiegoMovil.ViewModels.WebApi
{
    public class FicVmExportarWebApi : INotifyPropertyChanged
    {
        private string _FicTextAreaExpEdf;
        private ICommand _FicMetExpoEdf;

        private IFicSrvNavigation IFicSrvNavigationInventario;
        private IFicSrvExportarWebApi IFicSrvExportarWebApi;

        public FicVmExportarWebApi(IFicSrvNavigation IFicSrvNavigationInventario, IFicSrvExportarWebApi IFicSrvExportarWebApi)
        {
            this.IFicSrvNavigationInventario = IFicSrvNavigationInventario;
            this.IFicSrvExportarWebApi = IFicSrvExportarWebApi;
        }//CONSTRUCTOR

        public string FicTextAreaExpInv
        {
            get { return _FicTextAreaExpEdf; }
        }

        public async void OnAppearing()
        {

        }//AL INICIAR DE LA VIEW

        public ICommand FicMetExpoEdf
        {
            get
            {
                return _FicMetExpoEdf = _FicMetExpoEdf ??
                      new FicVmDelegateCommand(FicMetExportEdificio);
            }
        }//ESTE VENTO AGREGA EL COMANDO AL BOTON EN LA VIEW

        private async void FicMetExportEdificio()
        {
            try
            {
                _FicTextAreaExpEdf = await IFicSrvExportarWebApi.FicPostExportEdificios();
                RaisePropertyChanged("FicTextAreaExpInv");
                await new Page().DisplayAlert("ALERTA", "Datos Actualizados.", "OK");
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
            }
        }

        #region  INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }//CLASS
}//NAMESPACE

