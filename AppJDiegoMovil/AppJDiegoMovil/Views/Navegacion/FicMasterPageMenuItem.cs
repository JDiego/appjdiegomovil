﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppJDiegoMovil.Views.CatGenerales;

namespace AppJDiegoMovil.Views.Navegacion
{

    public class FicMasterPageMenuItem
    {
        public FicMasterPageMenuItem()
        {
            TargetType = typeof(FicViCatEdificiosList);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }

        public string Icon { get; set; }

        public string FicPageName { get; set; }

    }//CLASS
}//NAMESPACE