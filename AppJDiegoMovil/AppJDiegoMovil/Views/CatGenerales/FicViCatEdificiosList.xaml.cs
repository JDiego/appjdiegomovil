﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppJDiegoMovil.ViewModels.CatGenerales;
using System;
using AppJDiegoMovil.Services.CatGenerales;

namespace AppJDiegoMovil.Views.CatGenerales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCatEdificiosList : ContentPage
	{
        private object FicParameter { get; set; }
        FicSrvApp service { get; set; }

		public FicViCatEdificiosList()
		{
			InitializeComponent ();
            BindingContext = App.FicMetLocator.FicVmCatEdificiosList;
            this.FicParameter = null;
            service = new FicSrvApp();//FicSrvCatEdificosList
            //FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLiteNETStd>().FicGetDatabasePath());
		}

        //Command="{Binding FicMetDeleteCommand}"
        protected async void FicMetDeleteCommand(object sender, EventArgs e)
        {
            var context = BindingContext as FicVmCatEdificiosList;
            if (context.SfDataGrid_SelectItem_Edificio == null)
            {
                return;
            }
            bool conf = await DisplayAlert("Cuidado", "¿Desea eliminar este elemento?","Sí", "No");
            if (conf)
            {
                await service.FicMetRemoveEdificio(context.SfDataGrid_SelectItem_Edificio);
                context.SfDataGrid_ItemSource_Edificios.Remove(context.SfDataGrid_SelectItem_Edificio);
            }
            dataGrid.View.Refresh();
        }

        protected async void FicSearchButtonPressed(object sender, EventArgs e)
        {
            string filtro = FicSearchBar.Text;
            var source = BindingContext as FicVmCatEdificiosList;
            if (filtro == null || source.SfDataGrid_ItemSource_Edificios == null)
            {
                return;
            }
            filtro = filtro.ToLower();
            var resultEdificios = await service.FicMetGetListEdificios();
            source.SfDataGrid_ItemSource_Edificios.Clear();
            foreach (var edificio in resultEdificios)
            {
                if ((edificio.IdEdificio+"").ToLower().Contains(filtro) || (edificio.Alias + "").ToLower().Contains(filtro) ||
                    (edificio.DesEdificio + "").ToLower().Contains(filtro) || (edificio.Clave + "").ToLower().Contains(filtro))
                {
                    source.SfDataGrid_ItemSource_Edificios.Add(edificio);
                }
            }
            dataGrid.View.Refresh();
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosList;
            if (viewModel != null) viewModel.OnAppearing(FicParameter);
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosList;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}