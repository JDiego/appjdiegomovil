﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System;
using AppJDiegoMovil.Models.Asistencia;
using AppJDiegoMovil.ViewModels.CatGenerales;

namespace AppJDiegoMovil.Views.CatGenerales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCatEdificiosUpdate : ContentPage
	{
        private object FicLoParameter;

		public FicViCatEdificiosUpdate (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            Prior.Value = (FicParameter as eva_cat_edificios).Prioridad;
            BindingContext = App.FicMetLocator.FicVmCatEdificiosUpdate;
		}

        public void Handle_ValueChanged(object sender, Syncfusion.SfNumericUpDown.XForms.ValueEventArgs e)
        {
            (BindingContext as FicVmCatEdificiosUpdate).Edificio.Prioridad = Int16.Parse(e.Value.ToString());
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosUpdate;
            if (viewModel != null)
            {
                viewModel.OnAppearing(FicLoParameter);
            }
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosUpdate;
            if (viewModel != null)
            {
                viewModel.OnDisappearing();
            }
        }
    }
}