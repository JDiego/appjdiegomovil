﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppJDiegoMovil.ViewModels.CatGenerales;
using System;
using AppJDiegoMovil.Services.CatGenerales;

namespace AppJDiegoMovil.Views.CatGenerales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCatEdificiosItem : ContentPage
	{
        private object FicLoParameter { get; set; }
        FicSrvApp service { get; set; }

        public FicViCatEdificiosItem (object FicParameter)
		{
			InitializeComponent ();
            service = new FicSrvApp();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatEdificiosItem;
        }

        //Command="{Binding FicMetDeleteCommand}"
        protected async void FicMetDeleteCommand(object sender, EventArgs e)
        {
            var context = BindingContext as FicVmCatEdificiosItem;
            bool conf = await DisplayAlert("Cuidado", "¿Desea eliminar este elemento?", "Sí", "No");
            if (conf)
            {
                await service.FicMetRemoveEdificio(context.Edificio);
                context.BackNavgExecute();
            }
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosItem;
            if (viewModel != null)
            {
                viewModel.OnAppearing(FicLoParameter);
            }

        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosItem;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}