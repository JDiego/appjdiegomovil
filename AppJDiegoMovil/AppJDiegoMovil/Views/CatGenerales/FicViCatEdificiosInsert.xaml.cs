﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppJDiegoMovil.ViewModels.CatGenerales;
using System;

namespace AppJDiegoMovil.Views.CatGenerales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCatEdificiosInsert : ContentPage
	{
        private object FicLoParameter;

		public FicViCatEdificiosInsert (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatEdificiosInsert;
        }

        public void Handle_ValueChanged(object sender, Syncfusion.SfNumericUpDown.XForms.ValueEventArgs e)
        {
            (BindingContext as FicVmCatEdificiosInsert).Edificio.Prioridad = Int16.Parse(e.Value.ToString());
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosInsert;
            if (viewModel != null)
            {
                viewModel.OnAppearing(FicLoParameter);
            }

        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEdificiosInsert;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}