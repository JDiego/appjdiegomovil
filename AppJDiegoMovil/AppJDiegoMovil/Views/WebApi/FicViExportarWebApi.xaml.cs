﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppJDiegoMovil.ViewModels.WebApi;

namespace AppJDiegoMovil.Views.WebApi
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FicViExportarWebApi : ContentPage
    {
        public FicViExportarWebApi()
        {
            InitializeComponent();
            BindingContext = App.FicMetLocator.FicVmExportarWebApi;
        }//CONSTRUCTOR

        protected async override void OnAppearing()
        {
            var FicViewModel = BindingContext as FicVmExportarWebApi;
            if (FicViewModel != null)
            {
                FicViewModel.OnAppearing();
            }

        }//SE EJECUTA CUANDO SE ABRE LA VIEW

    }//CLASS
}//NAMESPACE