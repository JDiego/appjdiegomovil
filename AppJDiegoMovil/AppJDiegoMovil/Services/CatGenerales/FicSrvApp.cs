﻿using System.Collections.Generic;
using AppJDiegoMovil.Data;
using AppJDiegoMovil.Interfaces.CatGenerales;
using System.Threading.Tasks;
using AppJDiegoMovil.Models.Asistencia;
using AppJDiegoMovil.Helpers;
using Xamarin.Forms;
using AppJDiegoMovil.Interfaces.SQLite;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace AppJDiegoMovil.Services.CatGenerales
{
    public class FicSrvApp : IFicSrvApp
    {
        private static readonly FicAsyncLock ficMutex = new FicAsyncLock();
        private FicDBContext FicLoBDContext;

        public FicSrvApp()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLiteNETStd>().FicGetDatabasePath());
        }

        #region eva_cat_edificios
        public async Task FicMetInsertEdificio(eva_cat_edificios item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_cat_edificios.Add(item);
                await FicLoBDContext.SaveChangesAsync();
            }
        }

        public async Task<eva_cat_edificios> FicMetGetItemEdificioById(Int16 id)
        {
            var item = new eva_cat_edificios();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var r = (from FicCED in FicLoBDContext.eva_cat_edificios
                         where FicCED.IdEdificio == id
                         select new
                         { FicCED });

                item = r.ToArray()[0].FicCED;
            }
            return item;
        }

        public async Task<IList<eva_cat_edificios>> FicMetGetListEdificios()
        {
            var items = new List<eva_cat_edificios>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = await (from FicCED in FicLoBDContext.eva_cat_edificios
                                       select FicCED).AsNoTracking().ToListAsync();
                resultado.ToList().ForEach(x => items.Add(x));
            }
            return items;
        }

        public async Task FicMetUpdateEdificio(eva_cat_edificios item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.Entry(item).State = EntityState.Modified;
                await FicLoBDContext.SaveChangesAsync();
            }
        }

        public async Task FicMetRemoveEdificio(eva_cat_edificios item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.Entry(item).State = EntityState.Deleted;
                await FicLoBDContext.SaveChangesAsync();
            }
        }
        #endregion

    }
}
