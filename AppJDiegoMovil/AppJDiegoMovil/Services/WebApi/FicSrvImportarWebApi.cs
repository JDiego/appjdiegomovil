﻿using System.Threading.Tasks;
using AppJDiegoMovil.Interfaces.WebApi;
using AppJDiegoMovil.Data;
using System.Net.Http;
using Xamarin.Forms;
using AppJDiegoMovil.Interfaces.SQLite;
using Newtonsoft.Json;
using AppJDiegoMovil.Models.Asistencia;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AppJDiegoMovil.Services.WebApi
{
    public class FicSrvImportarWebApi : IFicSrvImportarWebApi
    {
        private readonly FicDBContext FicLoDBContext;
        private readonly HttpClient FiClient;

        public FicSrvImportarWebApi()
        {
            FicLoDBContext = new FicDBContext(DependencyService.Get<IFicConfigSQLiteNETStd>().FicGetDatabasePath());
            FiClient = new HttpClient();
            FiClient.MaxResponseContentBufferSize = 256000;
        }//CONSTRUCTOR

        public async Task<string> FicGetImportEdificios(int id = 0)
        {
            string FicMensaje = "IMPORTACION: \n";
            try
            {

                var FicGetResultREST = await (id != 0 ? FicGetListEdificioActualiza(id) : FicGetListEdificioActualiza());

                if (FicGetResultREST != null)
                {
                    FicMensaje += "IMPORTANDO: eva_cat_edificios \n";
                    foreach (eva_cat_edificios edf in FicGetResultREST)
                    {
                        var respuesta = await FicExisteva_cat_edificios(edf.IdEdificio);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdEdificio = edf.IdEdificio;
                                respuesta.Alias = edf.Alias;
                                respuesta.DesEdificio = edf.DesEdificio;
                                respuesta.Prioridad = edf.Prioridad;
                                respuesta.Clave = edf.Clave;
                                respuesta.FechaReg = edf.FechaReg;
                                respuesta.FechaUltMod = edf.FechaUltMod;
                                respuesta.UsuarioReg = edf.UsuarioReg;
                                respuesta.UsuarioMod = edf.UsuarioMod;
                                respuesta.Activo = edf.Activo;
                                respuesta.Borrado = edf.Borrado;
                                //FicLoDBContext.Update(respuesta);
                                FicLoDBContext.Entry(respuesta).State = EntityState.Modified;
                                FicMensaje += await FicLoDBContext.SaveChangesAsync() > 0 ? "-UPDATE -> IdEdificio: " + edf.IdEdificio + "\n" : "-NO NECESITO ACTUALIZAR -> IdEdificio: " + edf.IdEdificio + "\n";
                                FicLoDBContext.Entry(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception err)
                            {
                                FicMensaje += "-ALERTA 1 -> " + err.Message.ToString() + "\n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoDBContext.Add(edf);
                                FicMensaje += await FicLoDBContext.SaveChangesAsync() > 0 ? "-INSERT -> IdEdificio: " + edf.IdEdificio + "\n" : "-ERROR EN INSERT -> IdEdificio: " + edf.IdEdificio + "\n";
                            }
                            catch (Exception err)
                            {
                                FicMensaje += "-ALERTA 2 -> " + err.Message.ToString() + "\n";
                            }
                        }
                    }
                }
                else
                {
                    FicMensaje += "-> SIN DATOS.\n";
                }
            }
            catch (Exception err)
            {
                FicMensaje += "ALERTA 3 -> " + err.Message.ToString() + "\n";
            }
            return FicMensaje;
        }//FicGetImportEdificios()

        private async Task<IList<eva_cat_edificios>> FicGetListEdificioActualiza(int id = 0)
        {
            try
            {
                string URL = "http://localhost:63324/api/FicGetListCatEdificios" + (id != 0 ? $"/{id}" : "");
                var response = await FiClient.GetAsync(URL);
                return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<IList<eva_cat_edificios>>(await response.Content.ReadAsStringAsync()) : null;
            }
            catch (Exception err)
            {
                await new Page().DisplayAlert("ALERTA", err.Message.ToString(), "OKI");
                return null;
            }
        }//GET: EDIFICIOS

        private async Task<eva_cat_edificios> FicExisteva_cat_edificios(int id)
        {
            return await (from edf in FicLoDBContext.eva_cat_edificios where edf.IdEdificio == id select edf).AsNoTracking().SingleOrDefaultAsync();
        }
    }
}
