﻿using System.Threading.Tasks;
using AppJDiegoMovil.Interfaces.WebApi;
using AppJDiegoMovil.Data;
using System.Net.Http;
using Xamarin.Forms;
using AppJDiegoMovil.Interfaces.SQLite;
using Newtonsoft.Json;
using AppJDiegoMovil.Models.Asistencia;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace AppJDiegoMovil.Services.WebApi
{
    public class FicSrvExportarWebApi : IFicSrvExportarWebApi
    {
        private readonly FicDBContext FicLoBDContext;
        private readonly HttpClient FiClient;

        public FicSrvExportarWebApi()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLiteNETStd>().FicGetDatabasePath());
            FiClient = new HttpClient();
            FiClient.MaxResponseContentBufferSize = 256000;
        }//CONSTRUCTOR

        public async Task<string> FicPostExportEdificios()
        {
            return await FicPostExportEdificios(
                await (from edf in FicLoBDContext.eva_cat_edificios select edf).AsNoTracking().ToListAsync()
                );
        }//METODO DE EXPORT EDFICIOS

        private async Task<string> FicPostExportEdificios(IList<eva_cat_edificios> items)
        {
            const string URL = "http://localhost:63324/api/edficios/export";

            HttpResponseMessage response = await FiClient.PostAsync(
                    new Uri(string.Format(URL, string.Empty)),
                    new StringContent(JsonConvert.SerializeObject(items), Encoding.UTF8, "application/json")
                );
            return await response.Content.ReadAsStringAsync();
        }//POST: EDIFICIOS
    }//CLASS
}//NAMESPACE
